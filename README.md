# SCNU: Introduction to Statistics with Python

## Introduction 

This repository contains materials developed for an introductory unit in Statistics using Python. The materials are derived from a similar unit developed at [Curtin University](http://www.curtin.edu.au). 

## Slides

This folder contains the lecture slides and their source [LaTeX](https://ctan.org).  

## Python

The Python folder contains the notebooks and data that we will discuss in this unit. The [Anaconda distribution](https://docs.anaconda.com/anaconda/) of Python is probably one of the most convenience and [this article](https://medium.com/@neuralnets/beginners-quick-guide-for-handling-issues-launching-jupyter-notebook-for-python-using-anaconda-8be3d57a209b}) is also a helpful guide for beginner. 


