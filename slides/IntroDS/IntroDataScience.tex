\documentclass[envcountsect,10pt]{beamer}
\usepackage{curtin_beamer}
\usepackage{fcsymbol}
\usepackage{hyperref}
\title{Introduction to Data Science} 
\author[inst]{Felix Chan \inst{1}}
\institute[inst]{\inst{1} School of Accounting, Economics and Finance, Curtin University} 
\date{November 2019} 

\begin{document}
{\dotitle}
\begin{frame}
    \frametitle{Welcome!}
    \includegraphics[scale=0.35]{../pics/bigdata.jpg} \par
    \noindent Most of the materials can be found by clicking \href{https://gitlab.com/chansta/scnustatistics/}{here}!
\end{frame}
\begin{frame}
    \frametitle{What do these fancy terms actually mean...?} 
    \begin{columns}
	\begin{column}{0.48\linewidth}
	    \includegraphics[scale=0.2]{../pics/Big-Data-and-Analytics.jpg}
    \end{column}
    \begin{column}{0.48\linewidth}
	\begin{itemize}
	    \item Definition was unclear. 
	    \item Recently, people tend to classify big data as 
		\begin{itemize}
		    \item tall (many observations)
		    \item fat (many variables)
		    \item dense (high sampling frequency) 
		\end{itemize}
	\end{itemize}
    \end{column}
    \end{columns}
\end{frame} 
\begin{frame}
    \frametitle{Mighty Algorithms}
    \begin{center}
	\includegraphics[scale=0.2]{../pics/algorithms_xkcd1831.png}
    \end{center}
\end{frame}
\begin{frame}
    \frametitle{Data Analytics is old...}
    \begin{columns}
	\begin{column}{0.48\linewidth}
	    \small
	    \begin{enumerate}
		\item ``It is a capital mistake to theorize before one has data. Insensibly one begins to twist facts to suit theories, instead of theories to suit facts." - {\it A Scandal in Bohemia}
		\item ``I never guess. It is a shocking habit — destructive to the logical faculty." - {\it The Sign of the Four} 
		\item ``Data! Data! Data!’ he cried impatiently. ‘I can’t make bricks without clay." - {\it The Adventure of the Copper Beeches} 
		\item <2->``when you have eliminated the impossible, whatever remains, however improbable, must be the truth." - {\it The Sign of the Four}
	    \end{enumerate}
	\end{column}
	\begin{column}{0.48\linewidth}
	    \begin{center}
	    \includegraphics[scale=0.15]{../pics/sherlock.jpg}
	    \end{center}
	\end{column}
    \end{columns}
\end{frame}
\begin{frame}
    \frametitle{Decision based on data is also old...}
    \uncover<2->{\includegraphics[scale=0.4]{../pics/coleswool.jpg}}
    \uncover<3->{\includegraphics[scale=0.4]{../pics/carsales.jpg}}
    \uncover<4->{\includegraphics[scale=0.4]{../pics/housesales.jpg}}
\end{frame}
\begin{frame}
    \frametitle{Why the hype?} 
    \begin{center}
	\includegraphics[scale=0.5]{../pics/history.pdf}
    \end{center}
\end{frame} 
\setbeamercovered{transparent}
\begin{frame}
   \frametitle{Explosion of Data} 
    \noindent \href{https://techjury.net/blog/big-data-statistics/}{Some Statistics on Data}
    \begin{enumerate}
	\item <2-> \color<2>[rgb]{0.5,0,0.5} The big data analytics market is set to reach \$103 billion by 2023. 
	\item <3-> \color<3>[rgb]{0.5,0,0.5} In 2020, every person will generate 1.7 megabytes in just a second.
	\item <4-> \color<4>[rgb]{0.5,0,0.5} 97.2\% of organizations are investing in big data and AI.
	\item <5-> \color<5>[rgb]{0.5,0,0.5} Using big data, Netflix saves \$1 billion per year on customer retention
    \end{enumerate}
\end{frame} 
\begin{frame}
    \frametitle{Explosion of Data - Some questions}
   \begin{enumerate}
       \item <2-> \color<2>[rgb]{0.5,0,0.5} How do we find the data we need?
       \item <3-> \color<3>[rgb]{0.5,0,0.5} How can we extract the information we want, if it is there? 
       \item <4-> \color<4>[rgb]{0.5,0,0.5} How can we tell a story from these data? 
       \item <5-> \color<5>[rgb]{0.5,0,0.5} How do we translate or communicate these findings effectively? 
   \end{enumerate}
\end{frame} 
\setbeamercovered{invisible}
\begin{frame}
   \frametitle{A wise man once said...}
   \noindent {\it ``We are drowning in {\color{blue} information} and starving for {\color[rgb]{1,0,1} knowledge}"}\\ \raggedleft - Rutherford D. Roger  \par\vspace{1cm}\pause
    \raggedright \uncover<2->{\noindent {\it ``We are drowning in {\color{red} data} and starving for {\color[rgb]{1,0,1} knowledge}" } \\ \raggedleft - Felix Chan}
\end{frame}

\begin{frame}
    \frametitle{What can Data be used for?}
    \begin{itemize}
	\item Exploratory Data Analysis (EDA).
	\item Structural Modelling.
	\item Hypothesis Testing. 
    \end{itemize}    
\end{frame}
\begin{frame}
    \frametitle{Workflow} 
    \begin{center}
	\includegraphics[scale=0.2]{../pics/plan_workflow.pdf}
    \end{center}
\end{frame} 
\begin{frame}
    \frametitle{Workflow} 
    \begin{center}
	\includegraphics[scale=0.2]{../pics/actual_workflow.pdf}
    \end{center}
\end{frame} 
\begin{frame}
    \frametitle{Data Science}
    \begin{center}
	\includegraphics[scale=0.25]{../pics/venn.jpg}
    \end{center}
\end{frame}
\begin{frame}
    \frametitle{Team based approach}
    \begin{columns}
	\begin{column}{0.5\linewidth}
    \begin{enumerate}
	\item It could be a small team (team of 2) or multiple teams.  
	\item Communication and documentation becomes extremely important. 
	\item<2-> In fact, the latter is often the problem. 
    \end{enumerate}
	\end{column}
	\begin{column}{0.5\linewidth}
	    \includegraphics[scale=0.5]{../pics/team.jpg}
	\end{column}
    \end{columns}
\end{frame}
\begin{frame}
    \frametitle{Reproducibility}
    \begin{itemize}
	\item One of the motivations for communication and documentation is reproducibility. 
	\item Results must be reproducible. 
	\item Popular tools to combine documentation and reproducibility include \href{https://jupyter.org}{Jupyter Notebook} and \href{https://rmarkdown.rstudio.com/}{Rmarkdown}.  
	\item We will focus on Jupyter notebook in this unit. 
	\item We will also discuss a little bit about the \href{https://www.ands.org.au/working-with-data/fairdata}{F.A.I.R.} data principle. 
    \end{itemize}
\end{frame} 
\begin{frame}
    \frametitle{Data Visualisation} 
    \begin{itemize}
	\item Data Visualisation is important to summarise insights from `big data'. 
	\item It is also important to highlight abstract concepts. 
	\item Two examples here. 
	    \begin{itemize}
		\item \href{https://normaldist.herokuapp.com/}{Normal and $t$-distribution}
		\item \href{https://correlation-demon.herokuapp.com/}{Correlation}
	    \end{itemize}
    \end{itemize}
\end{frame}
\end{document}
