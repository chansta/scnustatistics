\documentclass[envcountsect,10pt]{beamer}
\usepackage{curtin_beamer}
\usepackage{fcsymbol}
\usepackage{hyperref}
\title{Modelling Uncertainty} 
\author[inst]{Felix Chan \inst{1}}
\institute[inst]{\inst{1} School of Accounting, Economics and Finance, Curtin University} 
\date{September 2018} 

\begin{document}
{\dotitle}

\begin{frame}
    \frametitle{Modelling Uncertainty}
    \noindent Data driven decision implies
    \begin{enumerate}
	\item outcome uncertainty
	\item imperfect information
    \end{enumerate}
    \uncover<2->{
    \begin{itemize}
	\item data can help with imperfect information
	\item data can help to reveal certain patterns (if exists) and hence improve outcome uncertainty 
    \end{itemize}}
    \uncover<3->{
    \noindent but it is extremely important to remember: 
    \begin{itemize}
	\item data is not information (noise)
	\item there needs not be identifiable pattern - don't look for a black cat in a dark room when it is not even there... 
    \end{itemize}}
\end{frame}
\begin{frame}
    \frametitle{Modelling uncertainty}
    \begin{itemize}
	\item So we need some tools to model uncertainty. 
	\item This forms the foundation of all the statistical techniques. 
	\item<2-> Probability 
	\item <3-> Random Variables
    \end{itemize}
\end{frame}
\begin{frame} 
	\frametitle{A Boring Example} 
	\begin{columns}
	    \begin{column}{0.5\linewidth}
		\begin{itemize}
			\item A boring example: coin tossing. 
			\item What is the probability of getting a head or tail? 
			\item Outcome is unknown but the all the {\it possible} outcomes are known, namely, head and tail. 
			\item Often we would conclude that the probability of getting a head or a tail is $1/2$. 
			\item Why? 
		\end{itemize} 
	    \end{column}
	    \begin{column}{0.5\linewidth}
		\begin{center}
		    \includegraphics[scale=0.5]{../pics/cointoss.jpg}
		\end{center}
	    \end{column}
	\end{columns}
\end{frame} 
\begin{frame}
	\frametitle{A Boring Example} 
	\begin{itemize}
		\item We are making the assumption that both outcomes have equal probability
		\item A reasonable assumption in the absence of information about the coin itself
		\item If the head side is heavier, then the tail side should appear more often when tossing a coin
		\item That is, in fact, the case. 
	\end{itemize} 
\end{frame} 
\begin{frame}
	\begin{itemize} 
	\frametitle{A Boring Example} 
		\item So what did we learn? 
		\item In this example, we do not know the exact outcome before the toss. However, we know all the possible outcomes and we seem to know the probability for occurrence of each outcome. 
		\item Define the set of all possible outcomes as $\Omega=\{H,T\}$, then the probability of $H$, denote $P(H)$, is essentially a number, between 0 and 1, associated with the event that $H$ occurs when tossing a coin. Similarly for $T$, $P(T)$ is the probability associated with the event that $T$ occurs when tossing a coin. Moreover, the probability of an outcome to occur must be 1, that is, $P(H)+P(T)=1$.  
	\end{itemize} 
\end{frame}
\begin{frame}
	\frametitle{Extending the Boring Example}
	\begin{itemize} 
		\item Consider the case when we are tossing two coins at the same time. 
		\item The set that contains all the possible outcomes are $\{ HH, HT, TT\}$. Let makes the assumption that each coin follows the following probability law: $P(H)=1/2$ and $P(T)=1/2$. 
		\item This implies that $P(HH)=P(TT)=1/4$ and $P(HT)=1/2$ (why)? 
		\item So what about $P(HH \text{ or } HT)$? Well, it is simply $P(HH) + P(HT) = 3/4$. 
		\item Note that $HH$ is an {\it outcome} but $HH \text{ or } HT$ is an {\it event}. 
	\end{itemize} 
\end{frame} 
\begin{frame} 
	\frametitle{Extending the Boring Example} 
	\begin{itemize}
		\item Why is $P(HH)=1/4$?
		\item If $A$ and $B$ are {\it independent} events, then $P(A \text{ and } B)$ (or $P(A \cup B)$) is equal to $P(A)P(B)$. So multiplication provides the probability of independent events happen at the same time. 
		\item Be very clear about the difference between {\it mutually exclusive} and {\it independent} events. 
		\item Mutually exclusive implies both events cannot happen at the same time, e.g. roll a dice gives 2 and 4.  
		\item The latter implies that observing the outcome of the first event provides no information about the probability of any outcome in the second event. However, both events can happen at the same time. E.g. roll a dice twice, the first roll gives 2 and second roll also gives 2.  
	\end{itemize} 
\end{frame} 
\begin{frame}
	\frametitle{Random Variables} 
	\begin{itemize} 
		\item From our Boring Example, we could let $Y_1$ be a random variable representing coin tossing so that 
		\begin{equation*}
			Y_1 = \begin{cases} 0 \quad \text{if head is the outcome} \\ 1 \quad \text{if tail is the outcome} \end{cases}
		\end{equation*}
	\end{itemize}
\end{frame} 
\begin{frame} 
	\frametitle{Random Variables}
	\begin{itemize}
		\item then the {\it probability mass function} associated with $Y$ is 
		\begin{equation*}
			P(Y_1) = \begin{cases} 1/2 \quad Y_1=0 \\ 1/2 \quad Y_1=1 \end{cases}
		\end{equation*}
	\end{itemize}
\end{frame} 
\begin{frame} 
	\frametitle{Random Variables} 
	\begin{itemize}
		\item If we let $Y_2$ be a random variable representing coin tossing with two coins so that 
		\begin{equation*}
			Y_2 = \begin{cases} 0 \quad HH \\ 1 \quad HT \\ 2 \quad TT \end{cases}
		\end{equation*}
		\item then the associated probability mass function 
		\begin{equation*}
			P\left ( Y_2 \right )= \begin{cases} \frac{1}{4} \quad Y_2 = 0 \\ \frac{1}{2} \quad Y_2 = 1 \\ \frac{1}{4} \quad Y_2 = 2 \end{cases} 
		\end{equation*}
	\end{itemize}
\end{frame} 
\begin{frame}
    \frametitle{Random Variables}
    \begin{itemize}
	\item Intuitively, we are simply assigning a number to an outcome. 
	\item Why do we want to do that? 
	\item <2-> With numbers come mathematical operators, e.g. addition, substraction, multiplication and division. 
	\item <3-> This means we can model more complex random event by combining simpler ones. 
    \end{itemize}
\end{frame}
\begin{frame} 
	\frametitle{Function of Random Variables} 
	\begin{itemize}
		\item Define $X_1$ as a random variable of tossing a fair coin and $X_2$ is a random variable tossing a second coin that is identical to the first one. 
		\item $X_1$ and $X_2$ are said to be {\it independently and identically distributed} random variables. 
		\item Independent because knowing the outcome of coin 1 tells us nothing about the outcome of coin 2. 
		\item identically distributed because both $X_1$ and $X_2$ follows exactly the same probability law and exactly the same set of outcomes. 
		\item Define $Y=X_1+X_2$. So what does this mean?  
	\end{itemize}
\end{frame} 
\begin{frame} 
	\frametitle{$Y=X_1+X_2$} 	
	\begin{itemize} 
		\item We know the possible values (outcomes) of $X_1$ is 0 or 1 and likewise for $X_2$. So the possible values (outcomes) for $Y$ are:
		\begin{equation*} 
			Y = \begin{cases} 0 \qquad X_1=0, X_2=0 \\ 1 \qquad X_1=1, X_2=0 \text{ or } X_1=0, X_2=1 \\ 2 \qquad X_1=1,X_2=1 \end{cases}
		\end{equation*} 
		\item From that, we could also work out the probability distribution for $Y$:
		\begin{equation*} 
			P(Y) = \begin{cases} \frac{1}{4} \qquad Y=0 \\ \frac{1}{2} \qquad Y=1 \\ \frac{1}{4} \qquad Y=2 \end{cases} 
		\end{equation*} 
	\end{itemize} 
\end{frame} 
\begin{frame} 
	\frametitle{$Y=X_1+X_2$} 
	\begin{itemize} 
		\item This is identical to the random variable of tossing two coins!! 
		\item Three important points about this example: 
		\item It is possible to combine any random variables through standard mathematical operations, like addition, multiplication.. etc. $Y$ is a function of random variables and it is itself, a random variable.
		\item However, the possible values of $Y$, even though is a function of the original random variables, is not the same as the original random variables. 
		\item More importantly, the probability distribution is also different. Sometimes we can work it out analytically, but more often than not, we couldn't but we could approximate them. 
	\end{itemize} 
\end{frame} 
\begin{frame}
	\frametitle{Basic Decision Theory via an Example}
		Consider an unfair coin with $P(H)=0.8$ and assuming that you will earn $\$ 1$ if you guess it right and loose $\$ 1$ if you guess it incorrectly. Over 5000 tosses, which of the following strategies will give you the best outcome? 
	\begin{enumerate}[a.]
	    \item Guess $H$ all the time. 
	    \item Guess $H$ 80\% of the time. 
	    \item Guess $H$ 50\% of the time. 
	    \item None of the above. 
	    \item <2-> We will model this probably later on. 
	\end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Discrete vs. Continuous}
    \begin{itemize}
	\item If the set of all possible outcomes are {\it countable}, then the random variable is {\it discrete} and the distribution of probabilities can be mathematically described by the {\it probability mass function}. 
	\item If the set of all possible outcomes are {\it uncountable}, then the random variable is {\it continuous} and the distribution of probabilities can be mathematically described by the {\it probability density function}.  
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Probability Mass Function}
    \begin{center}
	\includegraphics[scale=0.22]{../pics/binomial_30.pdf}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Probability Density Function}
    \begin{center}
	\includegraphics[scale=0.5]{../pics/normal.pdf}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Probability Density Function}
    \begin{center}
	\includegraphics[scale=0.5]{../pics/normal_prob.jpg}
    \end{center}
\end{frame}

\begin{frame} 
	\frametitle{Expectation}
	\noindent The {\it expectation} of a random variable, $X$, denotes $\E (X)$, is defined as 
	\begin{equation*}
		\E (X) = \sum^N_{i=1} x_iP\left (X=x_i \right )
	\end{equation*}
	\noindent if $X$ is a discrete random variable with $x_1,\cdots,x_n$ being the possible values for $X$ and 
	\begin{equation*}
		\E (X) = \int^b_a x f(x) dx
	\end{equation*}
	\noindent if $X$ is a continuous random variable with $(a,b)$ being the support of the probability density function, $f(x)$. 
\end{frame} 

\begin{frame} 
	\frametitle{Expectation - Numerical Examples}
	\noindent Let $X$ be a random variable representing the outcome of tossing a coin. $X=0$ if the outcome is head and $X=1$ if the outcome is tail. So the expectation of $Y$ is
	\begin{align*}
		\E (X) &= 0.P(X=0) + 1.P(X=1) \\
			&= 1.\frac{1}{2} \\
			&= \frac{1}{2}
	\end{align*}
\end{frame}

\begin{frame} 
	\frametitle{Expectation - Numerical Examples}
	\noindent Let $X$ be a random variable representing the outcome of rolling a fair dice. Assuming the dice is fair then the probability of each of the six outcomes, $\{1,2,3,4,5,6\}$, is 1/6. So the expectation of $X$ is
	\begin{align*}
		\E (X) &= \sum^6_{i=1} x_i P(X=x_i) \\
			&= \sum^6_{i=1} iP(X=i) \\
			&= 1 \frac{1}{6} + 2 \frac{1}{6} + 3 \frac{1}{6} + 4  \frac{1}{6} + 5  \frac{1}{6} + 6  \frac{1}{6} \\
			&= 3\frac{1}{2}
	\end{align*}
\end{frame} 

\begin{frame}
	\frametitle{Expectation - what does it mean?}
	\begin{itemize}
		\item The name would suggest that {\it expectation} gives you the ``expected outcome''.
		\item Can't be the case - the expectation calculated from the last two examples are not even in the outcome set! 
		\item Repeat experiments!!! 
		\item {\it expected} values over many many many repeated trails.
		\item $\E(X)$ is also called the {\it mean} of the random variable. 
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{A Challenge}
    \indent Let's assume you are making a sequence of investment decisions based on the movement of a particular stock price. The stock price can either go up or down and if you correctly predict the movement, you will gain $\rho_w$, otherwise you will loose $|\rho_l|$ i.e. $\rho_l < 0 $. How would you derive a strategy to optimize your gain? \par
\end{frame}

\begin{frame}
    \frametitle{A Challenge} 
    \begin{itemize}
	\item We can develop a {\it probability model} for this using random variable. \par
	\item Let $r$ be the random variable with two outcomes. $r=0$ when the price goes down and $r=1$ when the price goes up with $\Pr (r=0) = p_D$ and $\Pr (r=1) = 1-p_D = p_U$.  \par
	\item Let $s$ be a random variable with two outcomes denoting our prediction. $s=0$ and $s=1$ denote our prediction of price goes down and up, respectively. Our strategy depends on the frequency in which we predict up and down. That is $\Pr (s=0) = q_D$ forms our strategy of this game. 
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The Model}
    \begin{itemize}
	\item So when $r=s$ we make $\rho_w$ and when $r\neq s$ we lose $\rho_l$. 
	\item Let $z$ be a random variable represents the outcome of the investment.  
	\item The expected value of $z$ is  
	    \begin{equation*}
		\E (z) = \rho_w \Pr (r=s) + \rho_l \Pr (r\neq s) 
	    \end{equation*}
    \end{itemize}    

\end{frame}

\begin{frame}
    \frametitle{The Model}
    \begin{itemize}
	\item 
	    \begin{align*}
		\Pr (r=s) =&  \Pr (s=0) \Pr (r=0) + \Pr (r=1) \Pr (s=1) \\
			    =&  \Pr (s=0) \Pr (r=0) + \left [ 1- \Pr (r=0) \right ] \left [1- \Pr (s=0) \right ] \\
			    =& p_Dq_D + (1-p_d)(1-q_D)
	    \end{align*}
	\item
	    \begin{align*} 
		\Pr (r\neq s) =& \Pr (s=0) \Pr (r=1) + \Pr (s=1) \Pr (r=0)\\
		=& \Pr (s=0) \left [ 1- \Pr (r=0) \right ] + \Pr (r=0) \left [ 1- \Pr (s=0) \right ] \\
		=& q_D (1-p_D) + p_D(1-q_D).
	    \end{align*} 
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The Model}
    \noindent So the expected outcome, i.e. $\E (z) $ is 
    \begin{align*}
	\E (z) =& \rho_w \left [  p_Dq_D + (1-p_D)(1-q_D) \right ] + \rho_l \left [ q_D (1-p_D) + p_D(1-q_D) \right ] \\
	=& \rho_w \left ( 1- p_D - q_D + 2 p_D q_D \right ] + \rho_l \left [ q_D + p_D - 2p_Dq_D \right ] \\
	=& \rho_w (1-p_D) + \rho_w \left ( 2p_D -1 \right ) q_D + \rho_l p_D + \rho_l \left ( 1-2p_D \right ) q_D \\
	=&  \rho_w (1-p_D) + \rho_l p_D + \left [ \rho_w \left ( 2p_D -1 \right ) +  \rho_l \left ( 1-2p_D \right ) \right ] q_D\\ 
	=& \rho_w - \underbrace{(\rho_w- \rho_l) p_D}_{>0} + \underbrace{(\rho_w-\rho_l )(2p_D-1) q_D}_{<>0} 
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{The solution}
    \begin{equation*}
	\E (z) = \rho_w - \underbrace{(\rho_w- \rho_l) p_D}_{>0} + \underbrace{(\rho_w-\rho_l )(2p_D-1) q_D}_{<>0} 
    \end{equation*} 
    \begin{itemize}
	\item Need to determine when $\E (z) > 0$. 
	\item When $p_D=0.5$, none of the strategy matters. 
	\item When $p_D<0.5$, the last term is negative, so we want $q_D = 0$.
	\item When $p_D>0.5$, the last term is positive, so we want $q_D =1$. 
	\item Of course, this depends on the sign and magnitude of $\rho_w - (\rho_w- \rho_l) p_D$. 
	\item <2-> How do we know what $p_D$ is? 
    \end{itemize}    
\end{frame}
\begin{frame}
	\frametitle{Basic Decision Theory via an Example}
	\indent For this question $p_D$ is given to us. \par
	\indent Consider an unfair coin with $P(H)=0.8$ and assuming that you will earn $\$ 1$ if you guess it right and loose $\$ 1$ if you guess it incorrectly. Over 5000 tosses, which of the following strategies will give you the best outcome? 
	\begin{enumerate}[a.]
	    \item Guess $H$ all the time. 
	    \item Guess $H$ 80\% of the time. 
	    \item Guess $H$ 50\% of the time. 
	    \item None of the above. 
	\end{enumerate}
	\noindent What would be your answer? 
\end{frame}

\begin{frame}
    \frametitle{Data and Random variables}
    \begin{itemize}
	\item In practice we seldom have full information about our random variables.
	\item Specifically, we seldom know how the probabilities are {\it distributed}. 
	\item Data can help us estimate the distribution of probabilities. 
	\item Though often we don't need to know how all the probabilities are distributed. We may just want to know which outcomes are the most likely or less likely. 
	\item In other words, we may just want to focus on certain parts of the probability distributions - Moments!  (More about that in the next lecture). 
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Data and Random Variables}
    \noindent So how do we use data to estimate probability or moments? 
    \uncover<2->{
    \begin{theorem}[Law of Large Number]
	If $y_i$ are independently and identically distributed and $\E |y| < \infty$, then 
	\begin{equation}
		\frac{1}{N} \sum^N_{i=1} y_i \overset{p}{\rightarrow} \E (y) 
	\end{equation} 
    \end{theorem}
}
\end{frame} 

\begin{frame}
    \frametitle{Data and Random Variables}
    \small
    \begin{equation} 
	 \frac{1}{N} \sum^N_{i=1} y_i \approx  \E ( y ) 
     \end{equation}
     \noindent An interesting one: Let $I(A)=1$ if $A$ is true and 0 otherwise:  
     \begin{align*} 
	 \frac{1}{N} \sum^N_{i=1} I( x_i < K ) \approx& \E \left [ I(x<k) \right ] \\
						=& 1.P(x<k) + 0.P(x\geq k) \\
						=& P(x<k). 
    \end{align*}
    \noindent This shows we can use frequency of occurence to estimate probability i.e. histogram. 
 \end{frame}
\begin{frame}
	\frametitle{Conditional Probability} 
	\begin{itemize}
		\item One of the most important concepts in statistical modelling is {\it conditional probability}. 
		\item What is the probability of $A$ given $B$, $P(A|B)$? 
			\begin{equation*}
				P(A|B) = \frac{P(AB)}{P(B)} 
			\end{equation*} 
		\item It is the probability of observing $A$ assuming that $B$ has occurred.  
		\item <2-> What is the chance that RBA will increase interest rate if inflation is up (down)? 
		\item <3-> What is the chance that a Year 12 student who lives in Nedlands will select Curtin as his/her first preference? 
		\item <4-> What is the probability of of being over weight if the person is university educated, married and have three children? 
	\end{itemize} 
\end{frame}


\begin{frame}
	\frametitle{Conditional Probability}
	\begin{itemize} 
		\item So if the two events are independent, then $P(A|B)=P(A)$. 
		\item This is because observing $B$ does not inform us one way or another about the likelihood of observing $A$, ie knowing $B$ doesn't help. In that case.  
		\begin{align*}
			& P(A|B) = P(A) = \frac{P(AB)}{P(B)} \\		
			\Rightarrow & P(AB) = P(A)P(B) 
		\end{align*}
	    \item But if $P(A|B) \neq P(A)$ this means knowing $B$ gives us more {\it information} about $A$. 
	\end{itemize} 
\end{frame}

\begin{frame} 
	\frametitle{Conditional Probability}
		\begin{itemize}
			\item If $A$ and $B$ are mutually exclusive, then $P(AB)=0$, since they cannot both be happening at the same time. In that case. 
			\begin{align*} 
				P(A|B) &= \frac{P(AB)}{P(B)} \\
					&= 0
			\end{align*} 
			\item So if $A$ and $B$ are mutually exclusive, then knowing $B$ actually helps us to determine the likelihood of $A$.			\end{itemize}  
\end{frame} 
\begin{frame} 
	\frametitle{Bayes' Theorem}
	\begin{itemize}
		\item An extremely important theorem in probability and becoming more and more important in Econometrics and Machine Learning:
		\begin{equation*} 
			P(A|B) = \frac{P(B|A)P(A)}{P(B)} 
		\end{equation*} 
		\item It is essentially a result from Conditional Probability. 
		\begin{align*} 
			P(A|B) &= \frac{P(AB)}{P(B)} \\
			P(B|A) &= \frac{P(AB)}{P(A)} \\
		\Rightarrow P(A|B)P(B) &= P(B|A)P(A) = P(AB) \\
		\Rightarrow P(A|B) &= \frac{P(B|A)P(A)}{P(B)}
		\end{align*} 
	\end{itemize} 
\end{frame} 


\begin{frame}
    \frametitle{Conditional Expectation}
    \begin{itemize}
	\item This is the moment version of the conditional distribution!
	    \begin{align}
		\E (y | X=X_j ) =& \sum^n_{i=1} y_i P( y=y_i | X=X_j) \\
		\E (y | X ) =& \int y P(y|X) dy
	    \end{align}
	\item The expected value of random variable $y$ conditional on other random variables $X$. 
	\item In the case of first moment, this gives you the mean value of $y$ given the values of other variables $X$ - regression type analysis. 
	\item This forms the fundamental statistical objects for more advanced data analytics and statistical learning techniques. 
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{A little summary}
    \begin{columns}
	\begin{column}{0.5\linewidth}
    \begin{enumerate}
	\item Random variable as a tool to model uncertainty. 
	\item Knowing the possible outcomes and their likelihood (probabilities) can be help decision making. 
	\item The distirbution of probabilities is often unknown. 
	\item Data can help us estimating the probabilities.
	\item <2-> And/or the different characteristics of the distributions. 
    \end{enumerate}
	\end{column}
	\begin{column}{0.5\linewidth}
	    \begin{center}
		\includegraphics[scale=0.5]{../pics/sleep.jpg}
	    \end{center}
	\end{column}
    \end{columns}
\end{frame}
\end{document}
