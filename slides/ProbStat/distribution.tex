\documentclass[envcountsect,10pt]{beamer}
\usepackage{curtin_beamer}
\usepackage{fcsymbol}
\usepackage{hyperref}
\title{Normal and Other Related Distributions} 
\author[inst]{Felix Chan \inst{1}}
\institute[inst]{\inst{1} School of Economics, Finance and Property, Curtin University} 
\date{September 2018} 

\begin{document}
{\dotitle}

\begin{frame}
    \frametitle{Probability Distributions}
    \begin{columns}
	\begin{column}{0.5\linewidth}
	    \begin{itemize}
		\item Mathematically describes how the probabilities are being distributed among all the possible outcomes. 
		\item Often tedious mathematically, but conceptually is relatively straightforward. 
		\item The shape of the distribution can tell us a lot about the random variables. 
	    \end{itemize}
	\end{column}
	\begin{column}{0.5\linewidth}
	    \includegraphics[scale=0.2]{../pics/normal_prob.jpg}
	\end{column}
    \end{columns}
\end{frame} 

\begin{frame}
    \frametitle{Probability Distributions}
    \begin{itemize}
	\item The x-axis indicates the range or the type of possible outcomes. This is often called the {\it support} of the random variables. 
	\item In practical situation there are often two possibilities.
	    \begin{enumerate}
		\item The distribution can be derived based on a stochastic model, e,g, Model of passing a MCQ test by randomly guessing all the answers. 
		\item The problem is too difficult to analyse analytically and the distribution can only be approximated by data, e.g. stock prices. 
	    \end{enumerate}
	\item The latter is often the case in practice and this is the reason why data is so important. 
    \end{itemize}
\end{frame} 

\begin{frame}
    \frametitle{Descriptive Statistics}
    \begin{itemize}
	\item Descriptive statistics, such as mean, median, variance, skewness and kurtosis are sample estimates of the different characteristics of the underlying distributions. 
	\item To understand that, we need to introduce the notion of {\it statistical moments}. 
	\item This is done by extending our definition of {\it expectation}.
    \end{itemize}
\end{frame} 

\begin{frame} 
	\frametitle{Expectation}
	\noindent An important definition is that
		\begin{equation*}
			\E \left [ g(X) \right ] = \sum^n_{i=1} g(x) P(X=x)
		\end{equation*}
	\noindent in the discrete case and 
		\begin{equation*}
			\E \left [ g(X) \right ] = \int^b_a g(x)f(x) dx.
		\end{equation*}
	\noindent in the continuous case. 
\end{frame} 

\begin{frame} 
	\frametitle{Moment}
	\begin{itemize}
		\item An application of the above is the definition of moments. 
		\item Let $X$ be a random variable, then $\E (X^r)$ is called the $r^{th}$ raw moment of the random variable $X$, for all $r\in \Z^+$. 
		\item Let $X$ be a random variable representing the outcome of tossing a coin, then the second moment of $X$ is 
			\begin{align*}
				\E (X^2) &= 0^2P(X=0) + 1^2P(X=1) \\
				&= \frac{1}{2} 
			\end{align*} 
	\end{itemize}
\end{frame} 

\begin{frame}
	\frametitle{Moment - Numerical Example}
	\noindent Another example: Let $X$ be the random variable representing the outcome of rolling a dice, then the second moment of $X$ is 
	\begin{align*}
		\E \left (X^2 \right ) &= \sum^6_{i=1} x_i^2 P(X=x_i) \\
		&= \sum^6_{i=1} i^2P(X=i) \\
		&= 1^2.\frac{1}{6} + 2^2.\frac{1}{6} + 3^2.\frac{1}{6} + 4^2.\frac{1}{6} + 5^2.\frac{1}{6} + 6^2.\frac{1}{6}\\
		&= \frac{91}{6} = 15\frac{1}{6}.
	\end{align*}
\end{frame} 

\begin{frame}
	\frametitle{Centralised Moments}
	\noindent Let $X$ be a random variable, and let $m = \E(X)$ be the first moment (or the mean) of $X$, then the $r^{th}$ centralised moment is defined as
	\begin{equation*}
		\E \left [ (X-m)^r \right ] = \sum^n_{i=1} (x_i-m)^r P(X=x_i)
	\end{equation*}
	\noindent in the discrete case and 
	\begin{equation*}
		\E \left [ (X-m)^r \right ] = \int^b_a (x-m)^r f(x) dx
	\end{equation*}
	\noindent in the continuous. 
\end{frame} 

\begin{frame}
	\frametitle{Centralised Moments - Numerical Examples}
	\noindent Let $X$ be a random variable representing the outcome of rolling a fair dice. The variance of $X$ is the {\it second centralised moment} of $X$, which is 
	\begin{align*}
		\E \left [ (X-m)^2 \right ] =& \sum^6_{i=1} (x_i-m)^2 P(X=x_i) \\
					=& \sum^6_{i=1} (i-m)^2P(X=x_i) \\
					=& \left [ 1-3\frac{1}{2} \right ]^2.\frac{1}{6} + \left [ 2-3\frac{1}{2} \right ]^2.\frac{1}{6} + \left [ 3-3\frac{1}{2} \right ]^2.\frac{1}{6} \\ & + \left [ 4-3\frac{1}{2} \right ]^2.\frac{1}{6} + \left [ 5-3\frac{1}{2} \right ]^2.\frac{1}{6} + \left [ 6-3\frac{1}{2} \right ]^2.\frac{1}{6}.\\
					=& \frac{35}{12}
		\end{align*}
\end{frame} 

\begin{frame}
	\frametitle{Variance, Standard Deviation and Higher Order Moments}
	\begin{itemize}
		\item The standard deviation, by definition, is the square root of variance, ie standard deviation $=\sqrt{\text{variance}}$. 
		\item Standard deviation is a dispersion measure expressed in the same unit as the variable. 
		\item Variance is often more convenience from a mathematical perspective. 
		\item Higher Order moments also have important interpretations. Skewness, a measure of symmetry of distribution is a function of the third order moment and kurtosis, a measure of the thickness of tail of the distribution, is a function of fourth moment. 
	\end{itemize}
\end{frame} 

\begin{frame}
	\frametitle{Why moments?}
	\begin{itemize}
		\item It is hard to over estimate the importance of moments. It can be shown that if we have information for all moments, then we know everything there is to know about the probability distribution associated with the random variable. This is extremely important in empirical work as the true underlying distribution is often (more than often) unknown but we can estimate various moments given data!  
		\item The alternative is to assume the random variable follows a particular distribution but even so, there will be parameters in the distribution that we need to estimate based on the data. These parameters are often directly related to the moments of the distribution.
	\end{itemize} 
\end{frame} 

\begin{frame}
    \frametitle{Moments and Distribution}
    \begin{center}
	\includegraphics[scale=0.4]{../pics/normal_location.pdf}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Moments and Distribution}
    \begin{center}
	\includegraphics[scale=0.4]{../pics/normal_scale.pdf}
    \end{center}
\end{frame}

\begin{frame} 
	\frametitle{Random Variable in Practice} 
	\begin{itemize} 
		\item Imagine we have a random variable that captures the returns of tomorrow Apple Inc. stock. What is the probability of losing more than 2\% of your investment? 
		\item Probably quite low but how low? Can we use that information to manage our risk?  
		\item We don't really know, because we don't really know the exact probability distribution of Apple stock! 
	\end{itemize} 
\end{frame} 

\begin{frame}
	\frametitle{Random Variable in Practice}
	\begin{itemize} 
		\item The only way we could obtain the {\it exact} distribution is to collect data for the returns of Apple stock for the duration of the company life spans.  
		\item This is, however, infeasible. Also useless, even if it is feasible, as the company wouldn't have existed after all the data has been collected.  
		\item The second best solution is to collect the data on Apple returns over a period of time hoping this {\it sample} data gives us some idea about the {\it population}. 
		\item This is the fundamental idea about the concept of {\it population} and {\it random sampling}, which was discussed previously.
	\end{itemize} 
\end{frame} 

\begin{frame} 
	\frametitle{Random Sampling} 
	\begin{itemize} 
		\item Let $X$ be the random variable represents the return of tomorrow Apple stock, that is, $X\sim N(m,s^2)$.
		\item The problem is then to work out $m$ and $s^2$. 
	\end{itemize}
\end{frame} 

\begin{frame} 
	\frametitle{Random Sampling} 
	\begin{itemize} 
		\item If we have the data for the entire population, then in theory we could calculate {\it exactly} the values of $m$ and $s^2$ by 
			\begin{align*} 
				\E (X) &= m = \frac{1}{N} \sum^N_{i=1} X_i \\
				\text{Var} (X) &= s^2 = \frac{1}{N} \sum^N_{i=1} \left (X_i-m \right )^2 
			\end{align*}
			\noindent where $N$ is the size of the population. 
		    \item One can sense a problem here given this is a continuous random variable, so the notion of {\it population} is slightly problematic. We would over look that for now. 
		    \item In practice, we use the equations above to obtain the {\it sample estimates}. 
	\end{itemize} 
\end{frame} 
\begin{frame} 
	\frametitle{Other Distributions}
	\begin{itemize} 
		\item In your furture study, you will likely to encounter 4 more distributions, 3 of which derived from the normal distribution. 
	\item Let $X_i\sim N(0,1)$ and $Y_j\sim N(0,1)$ for $i=1...m$ and $j=1...n$. Assume that $X_i$ and $Y_j$ are independent, then 
	\end{itemize} 
\end{frame} 

\begin{frame}
	\frametitle{$\chi ^2$ Distribution} 
	\begin{equation*}
		Z_x = \sum^m_{i=1} X_i^2 \Rightarrow Z_x\sim \chi^2(m) \qquad
		Z_y = \sum^n_{j=1} Y_j^2 \Rightarrow Z_y \sim \chi ^2(n) 
	\end{equation*} 
	\includegraphics[width=10cm, height=5cm]{../pics/chi2.pdf}
\end{frame} 

\begin{frame}
	\frametitle{F Distribution}
	\begin{equation*}
			Z = \frac{Z_X^2}{Z_Y^2} \Rightarrow Z \sim F(m,n) 
	\end{equation*}
	\includegraphics[width=10cm, height=5cm]{../pics/fdist.pdf}
\end{frame} 

\begin{frame}
	\frametitle{t Distribution}
	\begin{equation*}
			Z = \frac{X_i}{\sqrt{Z_y/n}} \Rightarrow Z \sim t(n)
	\end{equation*}
	\includegraphics[width=10cm, height=5cm]{../pics/t5.pdf}
\end{frame} 




    \begin{frame}
	    \frametitle{Back to Normal Distribution} 
		\begin{itemize}
			\item The density of the normal distribution is  
				\begin{equation*}
					f(x) = \frac{1}{\sqrt{2\pi \sigma^2}} \exp \left [ -\frac{(x-\mu)^2}{2\sigma^2} \right ]
				\end{equation*} 
		\end{itemize}
		\includegraphics[width=10cm,height=4cm]{../pics/normal.pdf} 
	\end{frame} 
    \begin{frame} 
		\frametitle{A Graphical Introduction to Distribution} 
		\begin{itemize} 
			\item The x-axis contains the {\it support} (possible outcomes) of the distribution. 
			\item The area under the curve between an interval is the probability of any of the possible outcomes within that interval to occur. 
			\item $P(-1<x<0)$ equals the red area.
		\end{itemize}
		\includegraphics[width=10cm,height=4cm]{../pics/normal_prob.jpg}
	\end{frame} 
	\begin{frame}
		\frametitle{A Graphical Introduction to Distribution}
		\begin{itemize} 
			\item Let's look at the density function again.
				\begin{equation*}
					f(x) = \frac{1}{\sqrt{2\pi \sigma^2}} \exp \left [ -\frac{(x-\mu)^2}{2\sigma^2} \right ] 
				\end{equation*}
			\item The function depends on two {\it parameters}, namely $\mu$ and $\sigma$.
			\item Often we write $X \sim N(\mu,\sigma^2)$ to represent the random variable $X$ follows the normal distribution with $\mu$ and $\sigma^2$ as the two parameters. 
		\end{itemize} 
	\end{frame} 

	\begin{frame}
		\frametitle{A Graphical Introduction to Distribution}
		\begin{itemize}
			\item What do those parameters do? 
			\item $\mu$ changes the {\it location} of the distribution and it is often called the {\it location parameter} 
		\end{itemize} 
		\includegraphics[width=10cm, height=4cm]{../pics/normal_location.pdf}
	\end{frame}

	\begin{frame} 
		\frametitle{A Graphical Introduction to Distribution}
		\begin{itemize}
			\item $\sigma$ changes the {\it scale} of the distribution and it is often called the {\it scale parameter}
		\end{itemize}
		\includegraphics[width=10cm, height=4cm]{../pics/normal_scale.pdf}
	\end{frame} 

	\begin{frame}
		\frametitle{A Graphical Introduction to Distribution}
		\begin{itemize}
			\item Interestingly, if $X$ follows a normal distribution then the mean of $X$, (the first moment) is also the location parameter, $\mu$, of the associated normal distribution. Moreover, the variance (the second centralised moment) is the scale parameter, $\sigma^2$. 
			\item So the mean and the variance of the random variable determine the location and the scale of the normal distribution. 
			\item Importantly, the first two moments are all we need to determine all subsequent moments of the normal distribution.
			\item Moreover, the normal distribution is symmetric around the mean (skewness$=0$) and kurtosis$=3$. 
		\end{itemize} 
	\end{frame} 
	\begin{frame} 
		\frametitle{The Normal Distribution is Stable!}
		\begin{itemize} 
			\item An important feature of the normal distribution is that it belongs to a class of distributions called {\it stable distribution}. 
			\item Roughly speaking, it means that the sum of two independent normal random variates will also follow a normal distribution. 
			\item This is formally stated in the next proposition.  
		\end{itemize}
	\end{frame} 
	\begin{frame} 
		\frametitle{An Important Proposition} 
		\begin{proposition} \label{pro:sumnormal} 
				Let $X\sim N(\mu_x, \sigma^2_x)$ and $Y \sim N(\mu_y, \sigma^2_y)$ be two independent random variables, if $Z = aX + bY$ for any $a,b \in \R$ then 
			\begin{equation*} 
				Z \sim N \left ( a\mu_x + b\mu_y, a^2\sigma^2_x+b^2\sigma^2_y \right ). 
			\end{equation*} 
			\end{proposition}
	\end{frame}
	\begin{frame} 
		\frametitle{An Important Proposition}
		\begin{example} 
		Let $X\sim N(1,2)$, $Y\sim N(-1, 1)$ and $Z=X+2Y$. In this case, $a=1$, $b=2$, $\mu_x=1$, $\mu_y=-1$, $\sigma^2_x=2$ and $\sigma^2_y=1$, $Z$ is therefore normally distributed with mean $a\mu_x+b\mu_y = -1$ and variance $a^2\sigma^2_x + b^2\sigma^2_y = 2+4(1)=6$. That is $Z\sim N(-1,6)$.  
	\end{example} 

	\end{frame} 
	\begin{frame} 
		\frametitle{Calculating the Probabilities} 
		\small
		\begin{itemize} 
			\item Given $X\sim N(\mu,\sigma^2)$, the probability of $X$ lies between $a$ and $b$ is mathematically 
				\begin{equation*} 
					P(a<X<b) = \frac{1}{\sqrt{2\pi \sigma^2}} \int^b_a \exp \left [ -\frac{(x-\mu)^2}{2\sigma^2} \right ] dx
				\end{equation*}
			\item This is not easy as it does not have closed form solution and requires numerical approximations. 
			\item Luckily for us, $P(\infty < X < b)$ is tabulated for $X\sim N(0,1)$ with $b\in [-4,4]$. 
			\item See the file ``Ztable.pdf". 
			\item Most software can also generate these probabilities. 
		\end{itemize} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Calculating the Probabilities} 
		\small
		\begin{example} 
				Let $X\sim N(0,1)$, calculate $P(X < 0.52)$. In this case, $b=0.52$, so in the first column of the Table, locate the row that contains the number $0.5$, which is row 7 in the first column on page 2. We then locate the number $0.02$ in the first row, which is column 4 in the first row. The number at the intersection of row 7 and column 4 is 0.6985. This means $P(X<0.52)=0.6985$. 
		\end{example} 
		\begin{example} 
			Let $X\sim N(0,1)$, calculate $P(X > 0.52)$. Since the sum of probabilities is 1. This means $P(X > 0.52) = 1-P(X<0.52) = 1-0.6985 = 0.3015$. 
		\end{example} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Calculating the Probabilities} 
		\small
		\begin{example} 
			Let $X\sim N(0,1)$, calculate $P( -1 < X < 1.5)$. Note that in this case, we need to calculate the area under the normal density between -1 and 1.5. Note that $P(X<1.5)$ covers the area under the normal density from $-\infty$ to $x=1.5$ whereas $P(X<-1)$ covers the area under the normal density from $-\infty$ to $x=-1$. Therefore, $P(-1 < X < 1.5) = P( X< 1.5) - P(X<-1)$. Using the table, $P(X<-1)=0.1587$ and $P(X<1.5)=0.9332$, therefore $P(-1<X<1.5)=0.9332-0.1587=0.7745$.This is illustrated in Figure \ref{fig:normal_interval} where the yellow area denotes $P(X<-1)$ and the red area denotes $P(-1<X<1.5)$. The two area combined represent $P(X<1.5)$. Therefore the red area can be calculated as $P(X<1.5)-P(X<-1)$. 
		\end{example} 
	\end{frame}
	\begin{frame} 
		\frametitle{Calculating the Probabilities} 
			\begin{figure}[ht]
				\begin{center}
					\caption{Probability under $N(0,1)$ over the interval $[-1,1.5]$} \label{fig:normal_interval}
					\includegraphics[width=100mm, height=50mm]{../pics/normal_interval.pdf}
				\end{center}
			\end{figure} 
	\end{frame} 
	\begin{frame} 
		\frametitle{What about Different values of $\mu$ and $\sigma^2$?}
		\begin{itemize}
			\item So far we assume $X\sim N(0,1)$, that is $\mu=0$ and $\sigma^2=1$. 
			\item Given $Y\sim N(\mu,\sigma^2)$, how do we work out the probability for $P(a<Y<b)$? 
			\item Table book only provides the probability for $N(0,1)$ so we need to standardise our random variable. 
		\end{itemize}
	\end{frame} 

	\begin{frame}
		\frametitle{Standardising Random Variable}
		\small
		\noindent Let $Z$ be a random variable with 0 mean and unit variance, $Z\sim (0,1)$, and  
		\begin{equation*}
			X=\sigma Z + \mu
		\end{equation*} 
		\noindent with $\mu$ and $\sigma^2$ being constants then $X$ has mean $\mu$ and variance $\sigma^2$. Note, 
		\begin{equation*}
			Z = \frac{X-\mu}{\sigma}.
		\end{equation*}
		\noindent So in general, if $X$ is a random variable with mean $\mu$ and variance $\sigma^2$ and 
		\begin{equation*}
			Z = \frac{X-\mu}{\sigma}
		\end{equation*}
		\noindent then $Z$ has mean 0 and unit variance. This can be summarised in the following proposition. 
	\end{frame} 
	\begin{frame} 
		\frametitle{Standardising Random Variable} 
		\begin{proposition} \label{pro:standardised}  
			Let $X$ be a random variable with mean $\mu$ and variance $\sigma^2$. Let 
			\begin{equation}
				Z = \frac{X-\mu}{\sigma} \label{eq:standardised}
			\end{equation}
			\noindent then $Z$ has 0 mean and unit variance. That is $\E (Z) =0$ and $\E (Z^2) = 1$. 
		\end{proposition} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Standardising Random Variable} 
		\begin{itemize} 
			\item The proposition does not assume normality, which means it applies to all random variables with finite mean and variance. 
			\item We can use the proposition to convert any normal random variable to a standard normal random variable. 
			\item It also allows us to convert the calculation of probabilites for non-standard normal to a standard normal. 
		\end{itemize} 
		 \begin{align*} 
			P \left ( a < X < b \right ) =& P \left ( a-\mu < X-\mu < b-\mu \right ) \\
			=& P \left ( \frac{a-\mu}{\sigma} < \frac{X-\mu}{\sigma} < \frac{b-\mu}{\sigma} \right ) \\
			=& P \left ( \frac{a-\mu}{\sigma} < Z < \frac{b-\mu}{\sigma} \right ).
		\end{align*} 

	\end{frame} 
	\begin{frame} 
		\frametitle{Standardising Random Variable} 
		\begin{example} 
			Let $X\sim N(5,4)$, calculate $P(6<X<9)$. \par
			\noindent In this case, $\mu = 5$ and $\sigma^2 =4$, which implies $\sigma = 2$ and therefore 
			\begin{align*} 
				P \left ( 6 < X < 9 \right ) =& P \left ( \frac{6-5}{2} < Z < \frac{9-5}{2} \right ) \\
				=& P \left ( \frac{1}{2} < Z < 2 \right ). 
			\end{align*} 
			\noindent Since $Z\sim N(0,1)$, $P(0.5<Z<2) = P(Z<2)-P(Z<0.5) = 0.9772-0.6915=0.2857$. 
		\end{example} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Standardising Random Variable} 
		\small
		\begin{example} 
			Let $r_M\sim N(0.5,0.25)$ denotes the stock returns for Microsoft in 2015. What is the probabilities of making a profit from its stock and what is the probability its stock price drops by more than 1\%? \par
			Under the assumptions of the model, the probability of making a profit from Microsoft's stock is $P( r_M>0)=1-P(r_M<0)$. This gives:
			\begin{align*} 
				P \left ( r_M < 0 \right ) =& P \left ( \frac{r_M-0.5}{\sqrt{0.25}} < \frac{-0.5}{\sqrt{0.25}}\right ) \\
				=& P \left ( Z_M < -1\right ) \\
				=& 0.1587. 
			\end{align*}
			\noindent where $Z_M = \displaystyle \frac{r_M-0.5}{\sqrt{0.25}}$ represents the standardised $r_M$. 
		\end{example}
	\end{frame} 
	\begin{frame} 
		\frametitle{Example Cont.} 
		\small
		\begin{example} 
	 $P(r_M > 0) = 1-P(r_M<0) = 1-P(Z_M<-1)=1-0.1587= 0.8413$. Thus, under the assumption of the model, there is a 84\% chance that investing in Microsoft's stock will yield profit in 2015. \par
			In terms of the probability of observing a price drop, under the assumption of the model, this is equivalent to $P(r_M < -1)$ since $r_M$ represents the percentage change of Microsoft's stock price. This implies 
			\begin{align*} 
				P \left ( r_M < -1 \right ) =& P \left ( Z_M < \frac{-1-0.5}{\sqrt{0.25}} \right ) \\
				=& P \left ( Z_M < -3 \right ) \\
				=& 0.013. 
			\end{align*} 
			\noindent Thus there is 1.3\% chance that the price of Microsoft will drop more than 1\% under the assumptions of the model.
		\end{example} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Examine a Portfolio} 
		\small
		\begin{example} 
			Let $r_M\sim N(0.5, 0.25)$ and $r_G \sim N(-0.1, 0.1)$ denotes the stock returns for Microsoft and General Motor. Consider a portfolio consists of 80\% Microsoft stock and 20\% General Motor stock. Under the assumption that the returns of the two firms are independent, what is the probability of obtaining a profit from the portfolio and what is the probability of observing a 1\% drops in the value of the portfolio? \par
			\indent Let $R$ denotes the return of the portfolio then 
			\begin{equation*}
				R= 0.8r_M + 0.2r_G. 
			\end{equation*} 
			\noindent Since both $r_M$ and $r_G$ are normally distributed, under the assumption of independence, Proposition \ref{pro:sumnormal} implies that $R$ will be normally distributed with mean $0.8(0.5)+(0.2)(-0.1)=0.38$ and variance $0.8^2(0.25)+0.2^2(0.1)=0.164$. That is, $R\sim N(0.38,0.164)$. Therefore, the probability of observing a profit from the portfolio is $P(R>0) = 1-P(R<0)$. This means 
		\end{example} 
	\end{frame}
	\begin{frame} 
		\frametitle{Example Cont.} 
		\small
		\begin{example} 
			\begin{align*} 
				P \left ( R < 0 \right ) =& P\left ( Z_R < \frac{-0.38}{\sqrt{0.164}}\right ) \\
				=& P \left ( Z_R < -0.9383 \right ) 
			\end{align*} 
			\noindent where $Z_R = \displaystyle \frac{R-0.38}{\sqrt{0.164}}$ denotes the standardised portfolio returns. Note that $P(Z_R< -0.94) < P(Z<-0.9383) < P(Z<-0.93)$. This implies $ 0.1736< P(Z<-0.9383) < 0.1762$. This implies $0.8238<P(Z>-0.9383)<0.8264$ and hence, the probability of observing a profit from the portfolio is around 82\% to 83\% under the assumption of the model. 
		\end{example}
	\end{frame} 
	\begin{frame} 
		\frametitle{Example Cont.} 
		\begin{example} 
			Note that the imprecise nature of this analysis is partly due to the limitation of the table where it dose not provide probabilities for any boundary beyond 2 decimal places. Using scipy, we get a slightly more precise answer of $P(Z<-0.9838) =0.1740$, which implies $P(Z>-0.9838)=1-0.1740=0.8260$. 
		\end{example} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Example Cont.}
		\small
		\begin{example}
			\noindent The probability of observing a 1\% drop in the portfolio is $P(R<-1)$. This means:
			\begin{align*} 
				P(R<-1) =& P \left ( Z_R < \frac{-1-0.38}{\sqrt{0.164}} \right ) \\
				=& P \left ( Z_R < -0.3408 \right ) \\
				\approx & 0.0012. 
			\end{align*} 
			\noindent Therefore, the probability of observing a 1\% drop in the value is around 0.12\%. 
		\end{example} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Unknown $\mu$ and $\sigma^2$}
		\small
		\begin{itemize}
			\item So far we made the assumption that the actual mean and variance of the distribution is known. 
			\item This is seldom the case in practice. 
			\item Therefore, we want to be able to estimate $\mu$ and $\sigma^2$ from data. 
			\item The theory of estimation goes beyond obtaining a number, we also wants to assess the quality of the estimates. 
			\item In order to do that, we need a few more definitions. 
		\end{itemize} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Some Definitions} 
		\small
		\begin{definition} 
			 The statistic, $\hat{\theta}$, is an estimator of $\theta$ if it is a function of observations in a random sample used to estimate $\theta$. 
		 \end{definition} 
		\begin{definition}
			$\hat{\theta}$ is an unbiased estimator of $\theta$ if $\E (\hat{\theta} )=\theta$. 
		\end{definition} 
		\begin{definition} 
			A set of $N$ random variables $\{X_i\}^N_{i=1}$ is called a random sample from $X$ if $X_i$ is independent and identically distributed with the same probability distribution of $X$. Moreover, the distribution of $X$ is called the {\it parent} distribution. 
		\end{definition} 
	\end{frame} 
	\begin{frame} 
		\frametitle{An Unbiased Result} 
		\small
	\begin{proposition} \label{pro:unbiased} 
			Let $\{X_i\}^N_{i=1}$ be a random sample of $X\sim N(\mu, \sigma^2)$, then 
			\begin{equation} \label{eq:mean}
				\hat{\mu} = \frac{1}{N} \sum^N_{i=1} X_i
			\end{equation}
			\noindent and 
			\begin{equation}\label{eq:variance} 
				\hat{\sigma}^2 = \frac{1}{N-1} \sum^N_{i=1} \left (X_i - \hat{\mu} \right )^2
			\end{equation}
			\noindent are {\it unbiased estimators} of $\mu$ and $\sigma^2$, respectively. 
		\end{proposition}
	\end{frame} 	
	\begin{frame} 
		\frametitle{The Two Estimators} 
		\small
		\begin{itemize} 
			\item The proposition provides two estimators for the mean and variance of a normal random variable, respectively. 
			\item The proof, however, does not require normality, so these estimators can actually be used for non-normal random variates with finite mean and variance, as well. 
			\item The denominator $N-1$ in the equation of $\hat{\sigma}^2$ is the result of using $\hat{\mu}$ rather than $\mu$ in the estimation of the variance. If the true mean, $\mu$, is known, then a more {\it efficient} estimator can be used 
				\begin{equation*} 
					\tilde{\sigma}^2 = \frac{\sum^N_{i=1} \left (X_i -\mu \right )^2}{N}.
				\end{equation*} 
		\end{itemize}
	\end{frame} 
	\begin{frame} 
		\frametitle{The Two Estimators}
		\small
			\begin{example} \label{ex:example1}
				Let $\{ 4,3,1,4,5\}$ be a random sample of $X$. Given $N=5$, the mean of $X$ can be estimated as 
				\begin{align*} 
					\hat{\mu} =& \frac{\sum^N_{i=1} X_i}{N} \\
					=&\frac{4+3+1+4+5}{5} = 3.4.
				\end{align*} 
				\noindent Given $\hat{\mu}=3.4$, the variance of $X$ can be estimated as 
				\begin{align*} 
					\hat{\sigma}^2 =& \frac{\sum^N_{i=1} \left ( X_i - \hat{\mu} \right )^2}{N-1} \\
					=& \frac{ (4-3.4)^2 + (3-3.4)^2 + (1-3.4)^2 + (4-3.4)^2 + (5-3.4)^2}{5-1} \\
					=& \frac{9.2}{4} = 2.3.
				\end{align*} 
			\end{example} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Quality of the Estimator} 
		\small
		\begin{itemize} 
			\item The estimators are unbiased on expectation. 
			\item Surprisingly, it is possible to say something about the difference between the estimates and their true values. 
			\item The idea is to examine the variance of the estimators. 
			\item $\hat{\mu}$ is a random variable with mean equals to the true values, $\mu$. 
			\item Therefore, its variance, $\text{Var} (\hat{\mu}) = \E \left ( \hat{\mu} - \mu \right )^2$ can say something about the ``expected" deviation between the estimator and the true value. 
		\end{itemize} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Quality of the Estimator} 
		\begin{itemize} 
			\item It turns out that we do not need the true value $\mu$ to caluclate $\text{Var} \left ( \hat{\mu} \right )$. 
			\item Details can be found in the notes, but it turns out that
				\begin{equation*} 
					\text{Var} \left ( \hat{\mu} \right ) = \frac{\sigma^2}{N} 
				\end{equation*} 
			\item In the absence of the true value of $\sigma^2$, it can be replaced by the estimate of $\sigma^2$, $\hat{\sigma}^2$
				\begin{equation*} 
					\widehat{\text{Var}} \left ( \hat{\mu} \right ) = \frac{ \hat{\sigma}^2}{N}
				\end{equation*} 
			\item The square root of this variance is called the {\it standard error} of the estimator. 
		\end{itemize} 
	\end{frame} 
	\begin{frame} 
		\frametitle{Quality of the Estimator}  
			\begin{example} 
				Using the data from Example \ref{ex:example1}, $\hat{\sigma}^2=2.3$ with $N=5$, the standard error of $\hat{\mu}$ is therefore
				\begin{align*} 
					\text{S.E.} \left ( \hat{\mu} \right ) =& \sqrt{\frac{2.3}{5}} \\
					\approx & 0.6782. 
				\end{align*} 
			\end{example} 
	\end{frame} 
\end{document}


