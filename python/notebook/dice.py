##################################################################################################
"""
name:				dice.py
author:				Felix Chan
email:				fmfchan@gmail.com
date created:			2020.06.12
description:			
"""
##################################################################################################


import numpy as np 
import scipy.stats as sps
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def simulate(n, proll, pguess):
    """
    Simulate a set of random variables with binary outcomes. 
    Inputs:
        n: int. Number of games. 
        proll: float in (0,1) probability of 0 for the first random variable. 
        pguess: float in (0,1) probability of 0 for the second random variable. 
    Outputs:
        y: (n,) numpy array of 0 and 1 follows the law of proll. 
        x: (n,) numpy array of 0 and 1 follows the law of pguess.
        z: (n,) numpy array of |y-x|
    """
    rangen = sps.uniform.rvs(size=(2,n), loc=0, scale=1)
    y = rangen[0] < proll
    x = rangen[1] < pguess
    z = y.astype(int)-x.astype(int)
    return y,x,abs(z)

def outcome(win, lost, z, pguess, proll, disp=False):
    """
    Calculate the outcomes of the simulation. 
    Inputs:
        win: float. The amount of winning. 
        lost: float. The amount of lost. 
        proll: float in (0,1) probability of 0 in the actual outcome. 
        pguess: float in (0,1) probability of 0 in the investment decision (driven by strategy). 
        z: (n,) numpy array. The outcomes. Assume 0 means a win and 1 means a lost. 
    Outputs:
        actual: The actual gain/lost. 
        model: The theoretical gain/lost
    """
    n = z.shape[0]
    pwin = (pguess*proll) + (1-pguess)*(1-proll)
    plost = proll + pguess - 2*proll*pguess
    model = win*pwin + lost*plost
    outcomes = np.zeros(n)
    outcomes[z==1] = lost
    outcomes[z==0] = win
    if disp is True:
        print('The simulated outcome is {0:.3f} and the theoretical expectation is {1:.3f}'.format(np.mean(outcomes), model))
    return np.mean(outcomes), model

def random_generator(p, size=1): 
    """
        Simulate random outcomes of a dice based on the (accumulated) probability vector. 
        Inputs:
            p: (6,) random vector. 
            size: int. Number of observations to simulate
        Outputs:
            dice: (n,) numpy array of simulated outcomes. 
    """
    unif = sps.uniform.rvs(size=size)
    dice = np.empty(size)
    for i,u in enumerate(unif): 
        if u<p[0]:
            dice[i] = 1
        elif u<p[1]:
            dice[i] = 2
        elif u<p[2]:
            dice[i] = 3
        elif u<p[3]:
            dice[i] = 4
        elif u <p[4]:
            dice[i] = 5
        else:
            dice[i] = 6
    return dice

def gen_dice(markov=[4,2], size=5000):
    """
        Simulate a random variable for an unfair dice (six sided). 
        Inputs:
            markvo: (2,) array. The first number indicates the outcome which will change the probability of the subsequent outcomes. 
            size: int. Number of observations. 
        Output:
            dice: (n,) numpy array of dice outcomes.  
    """

    pvector = np.array([1/6, 1/12, 1/6, 1/3, 1/6, 1/6]) 
    pvector1 = np.ones(6)/12
    pvector1[markov[1]-1] = 7/12
    p = np.cumsum(pvector)
    p1 = np.cumsum(pvector1)
    dice = np.empty(size)
    for i in np.arange(size):
        if (i>0)&(dice[i-1]==4):
            dice[i] = random_generator(p1,size=1)
        else:
            dice[i] = random_generator(p,size=1)
    return dice



