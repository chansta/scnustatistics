import dice 

n = 1000
win, lost = 2, -2
pguess_set = np.arange(0,1,0.01)
proll_set = np.arange(0,1,0.01)
X,Y = np.meshgrid(pguess_set, proll_set)
result_actual = np.empty(X.shape)
result_model = np.empty(Y.shape)
for i, pg in enumerate(pguess_set): 
    for j, pr in enumerate(proll_set): 
        tempy,tempx,tempz = simulate(n, pr, pg)
        result_actual[i,j], result_model[i,j] = outcome(win,lost,tempz,pg, pr)

fig1 = plt.figure()
ax1 = fig1.add_subplot(111, projection='3d')
ax1.plot_surface(X,Y,result_model)
ax1.set_xlabel("Strategy")
ax1.set_ylabel("Roll")
plt.show()

